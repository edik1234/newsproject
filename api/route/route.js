module.exports = function (app) {
    const controller = require('../controller/controller');

    app.get('/getnews', controller.getnews);
    app.post('/addnews', controller.addnews)
}