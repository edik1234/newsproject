var tools = require('../tools/tools')

exports.getnew = function() {
    return new Promise((resolve) => {
        let sql = "SELECT * FROM news"
        tools.one(sql).then((resp) => {
            resolve(resp)
        })

    }) 
    .catch((e) => {
        console.error('Have problem2: ', e)
    })
}
exports.addnew = function(params) {
    return new Promise((resolve) => {
        let post = {name: `${params.name}`, discription: `${params.discription}`};
        let sql = 'INSERT INTO news SET ?'
        tools.two(sql, post).then((resp) => {
            resolve(resp)
        })
    })
    .catch((e) => {
        console.error('Have problem1: ', e)
    })
}
